<?php

$plugin = array(
  'label' => "Token type",
  'entity_save' => 'pathauto_multiple_token_type_property_entity_clear',
  'entity_delete' => 'pathauto_multiple_token_type_property_entity_clear',
  'default_widget' => 'pathauto_multiple_token_type_property_widget',
  'default_formatter' => 'pathauto_multiple_token_type_property_formatter',
);

/**
 * How to input a title.
 */
function pathauto_multiple_token_type_property_widget($property, $vars) {
  $entity = $vars['entity'];

  $token_info = token_info();
  $options = array('' => t('Please select'));
  foreach ($token_info['types'] as $type => $info) {
    $options[$type] = $info['name'];
    if (!empty($info['needs-data'])) {
      $options[$type] .= ' [needs: ' . $info['needs-data'] . ']';
    }
  }

  $token_type = array(
    '#type'   => 'select',
    '#title' => 'Token type',
    '#description' => t('The token type determines the availability of the object to be used within the alternative tokens below.'),
    '#options' => $options,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => !empty($entity->token_type) ? $entity->token_type : 0,
  );

  return $token_type;
}

function pathauto_multiple_token_type_property_formatter($property, $vars) {
  $entity = $vars['entity'];
  return array('#markup' => '<div class="token-type"><p>' . "<b>Token Type</b>: {$entity->token_type}" . '</p></div>');
}

/**
 * Store the current time in the property when the entity is changing.
 */
function pathauto_multiple_token_type_property_entity_clear($property, $vars) {
  token_clear_cache();
}
