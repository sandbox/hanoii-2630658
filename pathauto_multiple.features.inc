<?php
/**
 * @file
 * pathauto_multiple.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pathauto_multiple_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function pathauto_multiple_eck_bundle_info() {
  $items = array(
    'pathauto_multiple_alias_pathauto_multiple_alias' => array(
      'machine_name' => 'pathauto_multiple_alias_pathauto_multiple_alias',
      'entity_type' => 'pathauto_multiple_alias',
      'name' => 'pathauto_multiple_alias',
      'label' => 'Pathauto multiple alias',
      'config' => array(),
    ),
    'pathauto_multiple_pathauto_multiple' => array(
      'machine_name' => 'pathauto_multiple_pathauto_multiple',
      'entity_type' => 'pathauto_multiple',
      'name' => 'pathauto_multiple',
      'label' => 'Pathauto multiple',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function pathauto_multiple_eck_entity_type_info() {
  $items = array(
    'pathauto_multiple' => array(
      'name' => 'pathauto_multiple',
      'label' => 'Pathauto multiple',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'token' => array(
          'label' => 'Token',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'token_type' => array(
          'label' => 'Token type',
          'type' => 'text',
          'behavior' => 'token_type',
        ),
      ),
    ),
    'pathauto_multiple_alias' => array(
      'name' => 'pathauto_multiple_alias',
      'label' => 'Pathauto multiple alias',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
