<?php
/**
 * @file
 * pathauto_multiple.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pathauto_multiple_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'pathauto_multiple-pathauto_multiple-field_pathauto_multiple_desc'.
  $field_instances['pathauto_multiple-pathauto_multiple-field_pathauto_multiple_desc'] = array(
    'bundle' => 'pathauto_multiple',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The description on the token list.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple',
    'field_name' => 'field_pathauto_multiple_desc',
    'label' => 'Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple-pathauto_multiple-field_pathauto_multiple_label'.
  $field_instances['pathauto_multiple-pathauto_multiple-field_pathauto_multiple_label'] = array(
    'bundle' => 'pathauto_multiple',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The label on the token list.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple',
    'field_name' => 'field_pathauto_multiple_label',
    'label' => 'Label',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple-pathauto_multiple-field_pathauto_multiple_tokens'.
  $field_instances['pathauto_multiple-pathauto_multiple-field_pathauto_multiple_tokens'] = array(
    'bundle' => 'pathauto_multiple',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter alternative tokens that will be used to generate multiple aliases, one per line. <b>The last one will be served to pathauto and will be the default alias for the entity, this module will take care of the rest. Bear in mind that only the token types based on the pattern entity will be used.</b>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple',
    'field_name' => 'field_pathauto_multiple_tokens',
    'label' => 'Alternative tokens',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_alias'.
  $field_instances['pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_alias'] = array(
    'bundle' => 'pathauto_multiple_alias',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple_alias',
    'field_name' => 'field_pathauto_multiple_alias',
    'label' => 'Alias',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_language'.
  $field_instances['pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_language'] = array(
    'bundle' => 'pathauto_multiple_alias',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple_alias',
    'field_name' => 'field_pathauto_multiple_language',
    'label' => 'Language',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_pid'.
  $field_instances['pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_pid'] = array(
    'bundle' => 'pathauto_multiple_alias',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple_alias',
    'field_name' => 'field_pathauto_multiple_pid',
    'label' => 'Pid',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_source'.
  $field_instances['pathauto_multiple_alias-pathauto_multiple_alias-field_pathauto_multiple_source'] = array(
    'bundle' => 'pathauto_multiple_alias',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'pathauto_multiple_alias',
    'field_name' => 'field_pathauto_multiple_source',
    'label' => 'Source',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Alias');
  t('Alternative tokens');
  t('Description');
  t('Enter alternative tokens that will be used to generate multiple aliases, one per line. <b>The last one will be served to pathauto and will be the default alias for the entity, this module will take care of the rest. Bear in mind that only the token types based on the pattern entity will be used.</b>');
  t('Label');
  t('Language');
  t('Pid');
  t('Source');
  t('The description on the token list.');
  t('The label on the token list.');

  return $field_instances;
}
