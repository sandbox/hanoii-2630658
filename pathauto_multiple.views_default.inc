<?php
/**
 * @file
 * pathauto_multiple.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function pathauto_multiple_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'pathauto_multiple_aliases';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_pathauto_multiple_alias';
  $view->human_name = 'Pathauto multiple aliases';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Multiple aliases';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer pathauto';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Bulk operations: Pathauto multiple alias */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'eck_pathauto_multiple_alias';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Pathauto multiple alias: Alias */
  $handler->display->display_options['fields']['field_pathauto_multiple_alias']['id'] = 'field_pathauto_multiple_alias';
  $handler->display->display_options['fields']['field_pathauto_multiple_alias']['table'] = 'field_data_field_pathauto_multiple_alias';
  $handler->display->display_options['fields']['field_pathauto_multiple_alias']['field'] = 'field_pathauto_multiple_alias';
  $handler->display->display_options['fields']['field_pathauto_multiple_alias']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_pathauto_multiple_alias']['alter']['path'] = '[field_pathauto_multiple_alias]';
  /* Field: Pathauto multiple alias: Source */
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['id'] = 'field_pathauto_multiple_source';
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['table'] = 'field_data_field_pathauto_multiple_source';
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['field'] = 'field_pathauto_multiple_source';
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['label'] = 'System';
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_pathauto_multiple_source']['alter']['path'] = '[field_pathauto_multiple_source]';
  /* Field: Pathauto multiple alias: Language */
  $handler->display->display_options['fields']['field_pathauto_multiple_language']['id'] = 'field_pathauto_multiple_language';
  $handler->display->display_options['fields']['field_pathauto_multiple_language']['table'] = 'field_data_field_pathauto_multiple_language';
  $handler->display->display_options['fields']['field_pathauto_multiple_language']['field'] = 'field_pathauto_multiple_language';
  /* Field: Pathauto multiple alias: Delete link */
  $handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['table'] = 'eck_pathauto_multiple_alias';
  $handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['exclude'] = TRUE;
  /* Field: Pathauto multiple alias: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_pathauto_multiple_alias';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['exclude'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[edit_link] [delete_link]';
  /* Sort criterion: Pathauto multiple alias: Pid (field_pathauto_multiple_pid) */
  $handler->display->display_options['sorts']['field_pathauto_multiple_pid_value']['id'] = 'field_pathauto_multiple_pid_value';
  $handler->display->display_options['sorts']['field_pathauto_multiple_pid_value']['table'] = 'field_data_field_pathauto_multiple_pid';
  $handler->display->display_options['sorts']['field_pathauto_multiple_pid_value']['field'] = 'field_pathauto_multiple_pid_value';
  $handler->display->display_options['sorts']['field_pathauto_multiple_pid_value']['order'] = 'DESC';
  /* Filter criterion: Pathauto multiple alias: pathauto_multiple_alias type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_pathauto_multiple_alias';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'pathauto_multiple_alias' => 'pathauto_multiple_alias',
  );
  /* Filter criterion: Pathauto multiple alias: Alias (field_pathauto_multiple_alias) */
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['id'] = 'field_pathauto_multiple_alias_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['table'] = 'field_data_field_pathauto_multiple_alias';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['field'] = 'field_pathauto_multiple_alias_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['expose']['operator_id'] = 'field_pathauto_multiple_alias_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['expose']['label'] = 'Alias';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['expose']['operator'] = 'field_pathauto_multiple_alias_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['expose']['identifier'] = 'field_pathauto_multiple_alias_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_alias_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Pathauto multiple alias: Source (field_pathauto_multiple_source) */
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['id'] = 'field_pathauto_multiple_source_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['table'] = 'field_data_field_pathauto_multiple_source';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['field'] = 'field_pathauto_multiple_source_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['expose']['operator_id'] = 'field_pathauto_multiple_source_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['expose']['label'] = 'Source';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['expose']['operator'] = 'field_pathauto_multiple_source_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['expose']['identifier'] = 'field_pathauto_multiple_source_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_source_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Pathauto multiple alias: Language (field_pathauto_multiple_language) */
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['id'] = 'field_pathauto_multiple_language_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['table'] = 'field_data_field_pathauto_multiple_language';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['field'] = 'field_pathauto_multiple_language_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['expose']['operator_id'] = 'field_pathauto_multiple_language_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['expose']['label'] = 'Language';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['expose']['operator'] = 'field_pathauto_multiple_language_value_op';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['expose']['identifier'] = 'field_pathauto_multiple_language_value';
  $handler->display->display_options['filters']['field_pathauto_multiple_language_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/config/search/path/pathauto_multiple_aliases';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Multiple aliases';
  $handler->display->display_options['menu']['weight'] = '101';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['pathauto_multiple_aliases'] = $view;

  $view = new view();
  $view->name = 'pathauto_multiple_tokens';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_pathauto_multiple';
  $view->human_name = 'Pathauto multiple tokens';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Multiple tokens';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer pathauto';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['label'] = 'Action links';
  $handler->display->display_options['header']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['header']['area_text_custom']['content'] = '<ul class="action-links">
<li class="0 first last">
<a href="/admin/structure/entity-type/pathauto_multiple/pathauto_multiple/add?destination=admin/config/search/path/pathauto_multiple">Add Pathauto multiple</a>
</li>
</ul>';
  /* Field: Pathauto multiple: Token */
  $handler->display->display_options['fields']['token']['id'] = 'token';
  $handler->display->display_options['fields']['token']['table'] = 'eck_pathauto_multiple';
  $handler->display->display_options['fields']['token']['field'] = 'token';
  /* Field: Pathauto multiple: Token type */
  $handler->display->display_options['fields']['token_type']['id'] = 'token_type';
  $handler->display->display_options['fields']['token_type']['table'] = 'eck_pathauto_multiple';
  $handler->display->display_options['fields']['token_type']['field'] = 'token_type';
  /* Field: Pathauto multiple: Alternative tokens */
  $handler->display->display_options['fields']['field_pathauto_multiple_tokens']['id'] = 'field_pathauto_multiple_tokens';
  $handler->display->display_options['fields']['field_pathauto_multiple_tokens']['table'] = 'field_data_field_pathauto_multiple_tokens';
  $handler->display->display_options['fields']['field_pathauto_multiple_tokens']['field'] = 'field_pathauto_multiple_tokens';
  $handler->display->display_options['fields']['field_pathauto_multiple_tokens']['alter']['nl2br'] = TRUE;
  /* Field: Pathauto multiple: Delete link */
  $handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['table'] = 'eck_pathauto_multiple';
  $handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['label'] = '';
  $handler->display->display_options['fields']['delete_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['delete_link']['element_label_colon'] = FALSE;
  /* Field: Pathauto multiple: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_pathauto_multiple';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = '';
  $handler->display->display_options['fields']['edit_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[edit_link] [delete_link]';
  /* Filter criterion: Pathauto multiple: pathauto_multiple type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_pathauto_multiple';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'pathauto_multiple' => 'pathauto_multiple',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/config/search/path/pathauto_multiple';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Multiple tokens';
  $handler->display->display_options['menu']['weight'] = '100';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['pathauto_multiple_tokens'] = $view;

  return $export;
}
